# PROJETO PMX
## Autor: Henrique Amorim


## Sobre:
Projeto consiste no desenvolvimento de uma API-Rest para captura os dados em um campo de texto simples.
Os textos são enviados para um AWS-SQS, onde o Worker os armazena no MongoDB

## API
A API foi escrita em NodeJS por ser mais comum, não muito performático mas "dá pro gasto"!!!


## ITENS:

*TERRAFORM*

* Ferramenta usada para o provisionamento da infraestrutura e API;

*API-REST*

* Recebe os textos do usuário;
* Entrega os mesmos em uma fila no SQS;

*MONGODB*

* Armazena todos os textos provindos do WORKER;

*WORKER*

* Este os dados contidos na fila do SQS;
* Armazena os mesmos no banco de dados; 

# ORDEM DE EXECUÇÃO

1. Deverá criar um bucket de nome `maxmilhas-sqs-terraform`, este será utilizado para armazenar o TFSTATE;

2. No arquivo `docker-compose-infra.yml`, adicione `ACCESS-KEY,SECRET-KEY e REGION` correspondentes ao ambiente AWS a ser utilizado;

3. Provisionar infraestrutura SQS através do Terraform
```
docker-compose -f docker-compose-infra.yml up
```
4. Anotar a URL SQS após o provisionamento;

5. Adicionar as variáveis também no arquivo `docker-compose.yml`, incluindo credenciais AWS;

6. Provisionar Worker e API

```
docker-compose up --build -d
```

7. Acessar interface:

http://localhost