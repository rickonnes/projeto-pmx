variable "aws_region" {
    default = "us-east-2"
}

variable "aws_sqs_name" {
    default = "maxmilhas-sqs-terraform"
}

variable "aws_sqs_group" {
    default = "maxmilhas-terraform"
}
