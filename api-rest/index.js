const express = require('express');
const mongoose = require('mongoose');

// Load the AWS SDK for Node.js
const AWS = require('aws-sdk');
// Set the region 
AWS.config.update(
  {
    region: process.env.ARG_AWS_DEFAULT_REGION,
    accessKeyId: process.env.ARG_AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.ARG_AWS_SECRET_ACCESS_KEY
  });
// Create an SQS service object
const sqs = new AWS.SQS({apiVersion: '2012-11-05'});

const app = express();

app.set('view engine', 'ejs');

app.use(express.urlencoded({ extended: false }));

function sendMessageSQS(message){
  var params = {
    // Remove DelaySeconds parameter and value for FIFO queues
   DelaySeconds: 0,
   MessageAttributes: {
     "Title": {
       DataType: "String",
       StringValue: message
     }
   },
   MessageBody: message,
   MessageDeduplicationId: "Group1",  // Required for FIFO queues
   MessageGroupId: "Group1",  // Required for FIFO queues
   QueueUrl: process.env.ARG_SQS_QUEUE_URL
 };

 return new Promise((resolve, reject) => {
  sqs.sendMessage(params, function(err, data) {
    if (err) {
      console.log("Error", err);
      reject(err);
    } else {
      console.log("Success", data.MessageId);
      resolve(data);
    }
  });
 });
}

// Connect to MongoDB
mongoose
  .connect(
    'mongodb://mongo:27017/docker-node-mongo',
    { useNewUrlParser: true }
  )
  .then(() => console.log('MongoDB Connected'))
  .catch(err => console.log(err));

const Item = require('./models/Item');

app.get('/', (req, res) => {
  Item.find()
    .then(items => res.render('index', { items }))
    .catch(err => res.status(404).json({ msg: 'No items found' }));
});

app.post('/item/add', (req, res) => {
  const newItem = new Item({
    name: req.body.name
  });

  //newItem.save().then(item => res.redirect('/'));
  sendMessageSQS(req.body.name).then(data => res.redirect('/'));
});

const port = 3000;

app.listen(port, () => console.log('Server running...'));
