const express = require('express');
const mongoose = require('mongoose');

// Load the AWS SDK for Node.js
const AWS = require('aws-sdk');
// Set the region 
AWS.config.update(
  {
    region: process.env.ARG_AWS_DEFAULT_REGION,
    accessKeyId: process.env.ARG_AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.ARG_AWS_SECRET_ACCESS_KEY
  });
// Create an SQS service object
const sqs = new AWS.SQS({apiVersion: '2012-11-05'});

const app = express();

app.use(express.urlencoded({ extended: false }));

// Connect to MongoDB
mongoose
  .connect(
    'mongodb://mongo:27017/docker-node-mongo',
    { useNewUrlParser: true }
  )
  .then(() => console.log('MongoDB Connected'))
  .catch(err => console.log(err));

const Item = require('./models/Item');

var queueURL = process.env.ARG_SQS_QUEUE_URL;

var params = {
 AttributeNames: [
    "SentTimestamp"
 ],
 MaxNumberOfMessages: 10,
 MessageAttributeNames: [
    "All"
 ],
 QueueUrl: queueURL,
 VisibilityTimeout: 20,
 WaitTimeSeconds: 0
};

sqs.receiveMessage(params, function(err, data) {
  if (err) {
    console.log("Receive Error", err);
  } else if (data.Messages) {
    data.Messages.forEach(message => {
      const newItem = new Item({
        name: message.Body
      });
      newItem.save();
    });
  }
});

const port = 3001;

app.listen(port, () => console.log('Server running...'));
